module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    colors: {
      main: "#6a6464",
      black: "#000",
      grey: {
        light: "#ccc",
        dark: "#4b4543",
      },
      white: "#fff",
      beige: "#efebcf",
      blue: {
        light: "#3b475c",
        DEFAULT: "#27394c",
        dark: "#202b3c",
      },
    },
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
      },
    },
  },
  plugins: [],
};
