import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import PokemonView from "../views/PokemonView.vue";
import HelloWorld from "@/components/HelloWorld.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
      children: [
        {
          name: "detail",
          path: ":id",
          component: PokemonView,
        },
        {
          path: "",
          name: "empty",
          component: HelloWorld,
        },
      ],
    },
  ],
});

export default router;
