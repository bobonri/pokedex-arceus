import queryList from "./queries/list.txt?raw";
import querySingle from "./queries/single.txt?raw";

const API_URL = "https://beta.pokeapi.co/graphql/v1beta";

const defaultAggregateParam = {
  method: "POST",
  mode: "cors",
  headers: {
    "Content-Type": "application/json",
    "X-Method-Used": "graphql",
  },
};

export const fetchPokemons = async ({ offset = 100 }) => {
  try {
    const response = await fetch(API_URL, {
      ...defaultAggregateParam,
      body: JSON.stringify({ query: queryList.replace("[:offset]", offset) }),
    });
    if (response.ok) {
      return response.json();
    }
    return {};
  } catch (e) {
    console.log(e);
    return {};
  }
};

export const fetchPokemon = async ({ name }) => {
  if (name) {
    try {
      const response = await fetch(API_URL, {
        ...defaultAggregateParam,
        body: JSON.stringify({ query: querySingle.replace("[:name]", name) }),
      });
      if (response.ok) {
        return response.json();
      }
      return {};
    } catch (e) {
      console.log(e);
      return {};
    }
  }
  return {};
};
